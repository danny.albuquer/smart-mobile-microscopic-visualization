﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using OpenCvSharp;
using OpenCvSharp.XImgProc;
using Plugin.Media.Abstractions;

namespace WormDetector
{
    public class ImagePageModel
    {
        public ImagePageModel()
        {

        }

        public byte[] ProcessImage(MediaFile file)
        {
            using (var memoryStream = new MemoryStream())
            {
                file.GetStream().CopyTo(memoryStream);
                file.Dispose();

                byte[] imageData = memoryStream.ToArray();
                Mat img = Cv2.ImDecode(imageData, ImreadModes.Color);
                Mat temp = Cv2.ImDecode(imageData, ImreadModes.GrayScale);
                temp = Threshold(temp);
                Mat[] contours = Contours(temp);
                List<Rect> boundingBoxes = BoundingBoxes(contours);
                Cv2.DrawContours(img, contours, -1, new Scalar(255, 0, 0));
                foreach (Rect r in boundingBoxes)
                {
                    Cv2.Rectangle(img, r.TopLeft, r.BottomRight, new Scalar(0, 255, 0));
                }
                Mat skel = Skeletonize(temp);
                double alpha = 0.5;
                double beta = (1.0 - alpha);
                Cv2.CvtColor(skel, skel, ColorConversionCodes.GRAY2RGB);
                Cv2.AddWeighted(img, alpha, skel, beta, 0.0, img);

                return img.ToBytes(".jpg");
            }
        }
        public Mat Threshold(Mat input)
        {
            Mat output = new Mat();
            
            Cv2.AdaptiveThreshold(input, output, 255,
                    AdaptiveThresholdTypes.GaussianC, ThresholdTypes.BinaryInv, 201,
                    30);
            Mat kernel = Cv2.GetStructuringElement(MorphShapes.Ellipse,
                    new Size(10, 10));
            Cv2.MorphologyEx(output, output, MorphTypes.Close, kernel); // Fill holes

            return output;
        }

        public Mat[] Contours(Mat input)
        {
            Mat[] contours = Cv2.FindContoursAsMat(input, RetrievalModes.Tree, ContourApproximationModes.ApproxNone);
            return contours;
        }

        public List<Rect> BoundingBoxes(Mat[] contours)
        {
            List<Rect> boundingBoxes = new List<Rect>();

            for (int i = 0; i < contours.Length; i++)
            {
                Rect r = Cv2.BoundingRect(contours[i]);
                boundingBoxes.Add(r);
            }

            return boundingBoxes;
        }

        public Mat Skeletonize(Mat input)
        {
            Mat output = new Mat();
            CvXImgProc.Thinning(input, output, ThinningTypes.ZHANGSUEN);
            return output;
        }
    }
}
