﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using WormDetector;
using Xamarin.Forms;

/// <summary>
///  @author Danny Albuquerque Ferreira
/// </summary>
namespace WormDetector
{
    /// <summary>
    ///  Classe qui permet de récupérer et de sauvegarder les paramètres de l'utilisateur
    /// </summary>
    public class SettingsModel : INotifyPropertyChanged
    {

        public int MicroscopeZoom
        {
            get => Settings.MicroscopeZoom;
            set
            {
                if (Settings.MicroscopeZoom == value)
                    return;

                Settings.MicroscopeZoom = value;
                OnPropertyChanged();
            }
        }
        public bool Noise
        {
            get => Settings.Noise;
            set
            {
                if (Settings.Noise == value)
                    return;

                Settings.Noise = value;
                OnPropertyChanged();
            }
        }

        public bool Contours
        {
            get => Settings.Contours;
            set
            {
                if (Settings.Contours == value)
                    return;

                Settings.Contours = value;
                OnPropertyChanged();
            }
        }
        public bool BoundingBoxes
        {
            get => Settings.BoundingBoxes;
            set
            {
                if (Settings.BoundingBoxes == value)
                    return;

                Settings.BoundingBoxes = value;
                OnPropertyChanged();
            }
        }
        public bool Skeletons
        {
            get => Settings.Skeletons;
            set
            {
                if (Settings.Skeletons == value)
                    return;

                Settings.Skeletons = value;
                OnPropertyChanged();
            }
        }
        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string name = "") =>
          PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));

        #endregion
    }
}
