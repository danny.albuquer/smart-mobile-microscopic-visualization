﻿using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
/// <summary>
///  @author Danny Albuquerque Ferreira
/// </summary>
namespace WormDetector
{
    /// <summary>
    ///  Page principale de l'application
    /// </summary>
    public partial class MainPage : ContentPage
	{
        /// <summary>
        ///  Photo prise/sélectionnée par l'utilisateur 
        /// </summary>
        private MediaFile file;

        public MainPage()
        {
            InitializeComponent();
            CheckPermissionsAsync();

            // Prendre une photo
            btnTakePhoto.Clicked += async (sender, args) =>
            {
                if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                {
                    DisplayAlert("No Camera", ":( No camera avaialble.", "OK");
                    return;
                }
                file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
                {
                    PhotoSize = Plugin.Media.Abstractions.PhotoSize.Medium,
                    CompressionQuality = 50,
                    Directory = "Worms",
                    Name = "worms.jpg"
                });

                if (file == null)
                    return;

                ShowImagePageAsync();

            };

            // Charger une photo de la gallerie
            btnPickPhoto.Clicked += async (sender, args) =>
            {
                if (!CrossMedia.Current.IsPickPhotoSupported)
                {
                    DisplayAlert("Photos Not Supported", ":( Permission not granted to photos.", "OK");
                    return;
                }
                file = await Plugin.Media.CrossMedia.Current.PickPhotoAsync(new Plugin.Media.Abstractions.PickMediaOptions
                {
                    PhotoSize = Plugin.Media.Abstractions.PhotoSize.Medium,
                    CompressionQuality = 50
                });


                if (file == null)
                    return;

                ShowImagePageAsync();
                
            };

            // Afficher la page des paramètres
            toolbarItemSettings.Clicked += async (sender, args) =>
            {
                await Navigation.PushAsync(new SettingsPage());
            };
        }

        /// <summary>
        ///  Affiche la page qui doit afficher l'image traitée
        /// </summary>
        private async Task ShowImagePageAsync()
        {
            if (file != null)
            {
                ImagePage imagePage = new ImagePage(file);
                await Navigation.PushAsync(imagePage);
            }
        }

        /// <summary>
        ///  Vérifie si l'application à toutes les permissions dont elle a besoin
        /// </summary>
        private async Task CheckPermissionsAsync()
        {
            var cameraStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
            var storageStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);

            if (cameraStatus != PermissionStatus.Granted || storageStatus != PermissionStatus.Granted)
            {
                var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Camera, Permission.Storage });
                cameraStatus = results[Permission.Camera];
                storageStatus = results[Permission.Storage];
            }
        }
    }
}
