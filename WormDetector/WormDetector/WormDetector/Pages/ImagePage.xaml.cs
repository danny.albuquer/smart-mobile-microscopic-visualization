﻿using Plugin.Media.Abstractions;
using System;
using System.IO;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
/// <summary>
///  @author Danny Albuquerque Ferreira
/// </summary>
namespace WormDetector
{
    /// <summary>
    ///  Page qui affiche l'image traitée par l'API
    /// </summary>
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ImagePage : ContentPage
    {
        /// <summary>
        ///  Image source prise/sélectionnée par l'utilisateur
        /// </summary>
        private MediaFile File;
        /// <summary>
        ///  Helper permettant d'appeler l'API
        /// </summary>
        private RestServiceHelper RestServiceHelper;
        /// <summary>
        ///  Permet d'afficher le chargement de l'image
        /// </summary>
        private ActivityIndicator ActivityIndicator;

        public ImagePage(MediaFile file)
        {
            InitializeComponent();
            // Affiche le chargement de l'iamge
            ActivityIndicator = new ActivityIndicator()
            {
                IsRunning = true,
                IsVisible = true,
                Color = Color.FromHex("#90c5ff")
            };
            Func<RelativeLayout, double> getActivityIndicatorWidth = (parent) => ActivityIndicator.Measure(parent.Width, parent.Height).Request.Width;
            Func<RelativeLayout, double> getActivityIndicatorHeight = (parent) => ActivityIndicator.Measure(parent.Width, parent.Height).Request.Height;
            mainLayout.Children.Add(ActivityIndicator,
                Constraint.RelativeToParent(parent => parent.Width / 2 - getActivityIndicatorWidth(parent) / 2),
                Constraint.RelativeToParent(parent => parent.Height / 2 - getActivityIndicatorHeight(parent) / 2));

            RestServiceHelper = new RestServiceHelper();
            this.File = file;

            InitImageAsync();
        }

        /// <summary>
        ///  Appel l'API afin de récupérer l'image traitée et de l'afficher
        /// </summary>
        private async void InitImageAsync()
        {
            // Génère les paramètres en JSON
            RequestParams requestParams = new RequestParams()
            {
                MicroscopeZoom = Settings.MicroscopeZoom,
                Noise = Settings.Noise,
                Contours = Settings.Contours,
                Boundingboxes = Settings.BoundingBoxes,
                Skeletons = Settings.Skeletons
            };

            // Appel bloquant de l'API
            await RestServiceHelper.UploadImage(File.GetStream(), requestParams.ToJson());
            if (RestServiceHelper.Response != null)
            {
                // Conversion de l'image en base64 à un tableau de byte puis à une Image
                image.Source = ImageSource.FromStream(() =>
                {
                    byte[] imageArray = Convert.FromBase64String(RestServiceHelper.Response.Image);
                    var stream = new MemoryStream(imageArray);
                    return stream;
                });
                // Affiche le nombre de vers et cache le chargement
                nbWorms.Text = "Number of worms: " + RestServiceHelper.Response.NbWorms;
                nbWorms.IsVisible = true;
                ActivityIndicator.IsRunning = false;
                ActivityIndicator.IsVisible = false;
            }
            else
            {
                // En cas d'erreur, reviens à la page principale
                await Application.Current.MainPage.Navigation.PopAsync();
            }
        }
    }
}