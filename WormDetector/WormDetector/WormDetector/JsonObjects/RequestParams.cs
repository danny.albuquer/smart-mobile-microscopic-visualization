﻿using Newtonsoft.Json;

/// <summary>
///  @author Danny Albuquerque Ferreira
/// </summary>

namespace WormDetector
{
    /// <summary>
    ///  Représente le JSON contenant les paramètres pour le traitement d'images
    /// </summary>

    public partial class RequestParams
    {
        [JsonProperty("microscopezoom")]
        public int MicroscopeZoom { get; set; }

        [JsonProperty("noise")]
        public bool Noise { get; set; }

        [JsonProperty("contours")]
        public bool Contours { get; set; }

        [JsonProperty("boundingboxes")]
        public bool Boundingboxes { get; set; }

        [JsonProperty("skeletons")]
        public bool Skeletons { get; set; }
    }
    public static class Serialize
    {
        public static string ToJson(this RequestParams self) => JsonConvert.SerializeObject(self);
    }
}
