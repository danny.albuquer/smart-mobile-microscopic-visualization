﻿using Newtonsoft.Json;
/// <summary>
///  @author Danny Albuquerque Ferreira
/// </summary>
namespace WormDetector
{
    /// <summary>
    ///  Représente la réponse JSON de l'API
    /// </summary>
    public partial class ImageResponse
    {
        /// <summary>
        ///  Image en base64
        /// </summary>
        [JsonProperty("image")]
        public string Image { get; set; }

        /// <summary>
        ///  Nombre de vers sur l'image
        /// </summary>
        [JsonProperty("nbWorms")]
        public long NbWorms { get; set; }
    }

    public partial class ImageResponse
    {
        public static ImageResponse FromJson(string json) => JsonConvert.DeserializeObject<ImageResponse>(json);
    }
}