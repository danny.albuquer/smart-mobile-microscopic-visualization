﻿using Plugin.Settings;
using Plugin.Settings.Abstractions;
/// <summary>
///  @author Danny Albuquerque Ferreira
/// </summary>
namespace WormDetector
{
    /// <summary>
    ///  classe static permettant d'accéder aux paramètres de l'application
    /// </summary>
    public static class Settings
    {

        private static ISettings AppSettings =>
          CrossSettings.Current;

        public static int MicroscopeZoom
        {
            get => AppSettings.GetValueOrDefault(nameof(MicroscopeZoom), 0);
            set => AppSettings.AddOrUpdateValue(nameof(MicroscopeZoom), value);
        }

        public static bool Noise
        {
            get => AppSettings.GetValueOrDefault(nameof(Noise), false);
            set => AppSettings.AddOrUpdateValue(nameof(Noise), value);
        }

        public static bool Contours
        {
            get => AppSettings.GetValueOrDefault(nameof(Contours), true);
            set => AppSettings.AddOrUpdateValue(nameof(Contours), value);
        }

        public static bool BoundingBoxes
        {
            get => AppSettings.GetValueOrDefault(nameof(BoundingBoxes), true);
            set => AppSettings.AddOrUpdateValue(nameof(BoundingBoxes), value);
        }

        public static bool Skeletons
        {
            get => AppSettings.GetValueOrDefault(nameof(Skeletons), true);
            set => AppSettings.AddOrUpdateValue(nameof(Skeletons), value);
        }
    }
}