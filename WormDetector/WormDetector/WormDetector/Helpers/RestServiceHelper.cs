﻿using System;
using System.IO;
using System.Net.Http;

using System.Threading.Tasks;

/// <summary>
///  @author Danny Albuquerque Ferreira
/// </summary>
namespace WormDetector
{
    /// <summary>
    /// RestServiceHelper permet d'appeler la WormDetector-API
    /// </summary>
    public class RestServiceHelper
    {
        /// <summary>
        /// client Http permettant de faire les requêtes à l'API
        /// </summary>
        private HttpClient Client;

        /// <summary>
        /// URL de l'API
        /// </summary>
        public String RestUrl = "http://ps5_ferreira.tic.heia-fr.ch:8080/upload";

        /// <summary>
        /// Réponse de l'API contenant l'image et le nombre de vers
        /// </summary>
        public ImageResponse Response;


        /// <summary>
        /// Constructeur qui initialise le client HTTP 
        /// </summary>
        public RestServiceHelper()
        {
            Client = new HttpClient();
        }

        /// <summary>
        /// Envoye l'image à l'API avec les paramètres souhaités et place le résultat dans Response
        /// </summary>
        /// <param name="stream">Stream de l'image source</param>  
        /// <param name="jsonParams">Paramètres pour le traitement d'images</param>  
        public async Task<bool> UploadImage(Stream stream, string jsonParams)
        {
            // Création du contenu encodé pour la requête HTTP
            MultipartFormDataContent form = new MultipartFormDataContent();

            // Ajout des paramètres en JSON
            var stringContent = new StringContent(jsonParams);
            stringContent.Headers.Add("Content-Disposition", "form-data; name=\"params\"");
            form.Add(stringContent, "params");

            // Ajout de l'image à traiter
            var streamContent = new StreamContent(stream);
            streamContent.Headers.Add("Content-Type", "application/octet-stream");
            streamContent.Headers.Add("Content-Disposition", "form-data; name=\"file\"; filename=\"image.jpg\"");
            form.Add(streamContent, "file", "image.jpg");

            try
            {
                // Appel à l'API
                var response = await Client.PostAsync(RestUrl, form);
                if (response.IsSuccessStatusCode) // Si OK, le résultat est stocké dans Response
                {
                    this.Response = ImageResponse.FromJson(response.Content.ReadAsStringAsync().Result);
                    return true;
                }
                else
                {
                    this.Response = null;
                    return false;
                }
            }catch(Exception e)
            {
                return false;
            }
        }
    }
}
