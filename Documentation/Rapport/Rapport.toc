\select@language {french}
\ttl@change@i {\@ne }{chapter}{1.5em}{\addvspace {1em plus 0pt}\bfseries }{\contentslabel {1.3em}}{\hspace {-1.3em}}{\hfill \contentspage }\relax 
\ttl@change@v {chapter}{}{}{\addvspace {0pt}}\relax 
\ttl@change@i {\@ne }{section}{3.8em}{\addvspace {0pt}}{\contentslabel {2.3em}}{\hspace *{-2.3em}}{\titlerule *[0.75em]{.}\contentspage }\relax 
\ttl@change@v {section}{}{}{\addvspace {0pt}}\relax 
\ttl@change@i {\@ne }{subsection}{7.0em}{\addvspace {0pt}}{\contentslabel {3.2em}}{\hspace *{-3.2em}}{\titlerule *[0.75em]{.}\contentspage }\relax 
\ttl@change@v {subsection}{}{}{\addvspace {0pt}}\relax 
\contentsline {chapter}{\numberline {1}Introduction}{4}{chapter.1}
\contentsline {section}{\numberline {1.1}Structure}{5}{section.1.1}
\contentsline {chapter}{\numberline {2}Analyse}{6}{chapter.2}
\contentsline {section}{\numberline {2.1}\IeC {\'E}tat du march\IeC {\'e}}{6}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}WormTracker 2.0}{6}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}The Parallel Worm Tracker}{7}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}WormLab}{7}{subsection.2.1.3}
\contentsline {section}{\numberline {2.2}Configurations mat\IeC {\'e}rielles possibles}{8}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Application mobile}{8}{subsection.2.2.1}
\contentsline {subsubsection}{Avantages}{8}{subsubsection*.3}
\contentsline {subsubsection}{D\IeC {\'e}savantages}{8}{subsubsection*.4}
\contentsline {subsection}{\numberline {2.2.2}Application desktop}{9}{subsection.2.2.2}
\contentsline {subsubsection}{Avantages}{9}{subsubsection*.6}
\contentsline {subsubsection}{D\IeC {\'e}savantages}{9}{subsubsection*.7}
\contentsline {subsection}{\numberline {2.2.3}Restful API}{10}{subsection.2.2.3}
\contentsline {subsubsection}{Avantages}{10}{subsubsection*.9}
\contentsline {subsubsection}{D\IeC {\'e}savantages}{10}{subsubsection*.10}
\contentsline {section}{\numberline {2.3}Traitement d'images}{11}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Technologies}{11}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}\IeC {\'E}tapes du traitement d'images}{12}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Binarisation}{12}{subsection.2.3.3}
\contentsline {subsubsection}{Seuillage fixe}{13}{subsubsection*.12}
\contentsline {subsubsection}{M\IeC {\'e}thode d'Otsu}{13}{subsubsection*.14}
\contentsline {subsubsection}{Seuillage adaptatif}{14}{subsubsection*.16}
\contentsline {subsection}{\numberline {2.3.4}D\IeC {\'e}tection de contours}{14}{subsection.2.3.4}
\contentsline {subsection}{\numberline {2.3.5}Bo\IeC {\^\i }tes englobantes}{14}{subsection.2.3.5}
\contentsline {subsection}{\numberline {2.3.6}Squelettisation}{15}{subsection.2.3.6}
\contentsline {subsubsection}{Op\IeC {\'e}rations morphologiques}{15}{subsubsection*.19}
\contentsline {subsubsection}{Algorithme de Zhang-Suen}{17}{subsubsection*.22}
\contentsline {subsubsection}{Descripteur de forme}{18}{subsubsection*.24}
\contentsline {subsection}{\numberline {2.3.7}Cas particuliers}{18}{subsection.2.3.7}
\contentsline {subsubsection}{Chevauchements de vers}{18}{subsubsection*.26}
\contentsline {subsubsection}{Enroulements}{19}{subsubsection*.28}
\contentsline {section}{\numberline {2.4}Application mobile}{20}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Cas d'utilisation}{20}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Technologies}{20}{subsection.2.4.2}
\contentsline {subsubsection}{Application native}{20}{subsubsection*.31}
\contentsline {subsubsection}{Xamarin}{20}{subsubsection*.32}
\contentsline {paragraph}{Xamarin.Forms}{21}{paragraph*.33}
\contentsline {paragraph}{.NET Standard}{21}{paragraph*.35}
\contentsline {section}{\numberline {2.5}Synth\IeC {\`e}se}{22}{section.2.5}
\contentsline {chapter}{\numberline {3}Conception}{23}{chapter.3}
\contentsline {section}{\numberline {3.1}Configuration mat\IeC {\'e}rielle}{23}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Diagramme de d\IeC {\'e}ploiement}{23}{subsection.3.1.1}
\contentsline {section}{\numberline {3.2}Application mobile}{24}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Maquettes}{24}{subsection.3.2.1}
\contentsline {subsubsection}{Vue principale}{24}{subsubsection*.38}
\contentsline {subsubsection}{Vue de prise de photo}{24}{subsubsection*.40}
\contentsline {subsubsection}{Vue de s\IeC {\'e}lection dans la galerie d'image}{25}{subsubsection*.42}
\contentsline {subsubsection}{Vue des param\IeC {\`e}tres}{25}{subsubsection*.44}
\contentsline {subsubsection}{Vue de l'image trait\IeC {\'e}e}{26}{subsubsection*.46}
\contentsline {subsection}{\numberline {3.2.2}Interface graphique}{26}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}R\IeC {\'e}cup\IeC {\'e}ration de l'image}{27}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Param\IeC {\`e}tres}{27}{subsection.3.2.4}
\contentsline {subsection}{\numberline {3.2.5}Appel de l'API}{27}{subsection.3.2.5}
\contentsline {section}{\numberline {3.3}API}{27}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Requ\IeC {\^e}tes HTTP}{27}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Algorithme de traitement d'images}{28}{subsection.3.3.2}
\contentsline {subsubsection}{Binarisation}{28}{subsubsection*.49}
\contentsline {subsubsection}{Contours}{29}{subsubsection*.50}
\contentsline {subsubsection}{Bo\IeC {\^\i }tes englobantes}{30}{subsubsection*.51}
\contentsline {subsubsection}{Squelettisation}{30}{subsubsection*.52}
\contentsline {subsubsection}{Comptage des vers}{30}{subsubsection*.53}
\contentsline {section}{\numberline {3.4}Synth\IeC {\`e}se}{30}{section.3.4}
\contentsline {chapter}{\numberline {4}Impl\IeC {\'e}mentation}{31}{chapter.4}
\contentsline {section}{\numberline {4.1}Application mobile}{31}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Diagramme de classes}{31}{subsection.4.1.1}
\contentsline {subsubsection}{MainPage}{32}{subsubsection*.55}
\contentsline {subsubsection}{ImagePage}{33}{subsubsection*.56}
\contentsline {subsubsection}{SettingsPage}{33}{subsubsection*.57}
\contentsline {subsubsection}{Settings}{33}{subsubsection*.58}
\contentsline {subsubsection}{RestServiceHelper}{33}{subsubsection*.59}
\contentsline {subsection}{\numberline {4.1.2}Appel \IeC {\`a} l'API}{33}{subsection.4.1.2}
\contentsline {subsubsection}{G\IeC {\'e}n\IeC {\'e}ration de la requ\IeC {\^e}te}{33}{subsubsection*.60}
\contentsline {subsubsection}{Envoie de la requ\IeC {\^e}te}{34}{subsubsection*.61}
\contentsline {subsection}{\numberline {4.1.3}R\IeC {\'e}sultats}{34}{subsection.4.1.3}
\contentsline {section}{\numberline {4.2}API}{35}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Diagramme de classes}{35}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Traitement des requ\IeC {\^e}tes}{35}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Traitement d'images}{36}{subsection.4.2.3}
\contentsline {subsubsection}{Binarisation}{36}{subsubsection*.64}
\contentsline {subsubsection}{Contours}{37}{subsubsection*.65}
\contentsline {subsubsection}{Bo\IeC {\^\i }tes englobantes}{37}{subsubsection*.66}
\contentsline {subsubsection}{Squelettes}{37}{subsubsection*.67}
\contentsline {subsection}{\numberline {4.2.4}Versions}{38}{subsection.4.2.4}
\contentsline {section}{\numberline {4.3}Tests}{38}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Tests application mobile}{38}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Tests du traitement d'images}{38}{subsection.4.3.2}
\contentsline {subsubsection}{R\IeC {\'e}sultats}{39}{subsubsection*.68}
\contentsline {chapter}{\numberline {5}Conclusion}{40}{chapter.5}
\contentsline {subsection}{\numberline {5.0.1}Future}{40}{subsection.5.0.1}
\contentsline {section}{\numberline {5.1}D\IeC {\'e}claration d'honneur}{40}{section.5.1}
\contentsline {chapter}{\numberline {6}Structure du DVD}{41}{chapter.6}
\contentsfinish 
