package wormdetector.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WormDetectorApplication {

    public static void main(String[] args) {
        SpringApplication.run(WormDetectorApplication.class);
        nu.pattern.OpenCV.loadShared();
    }

}