package wormdetector.api.controllers;

import wormdetector.api.models.WormDetectorModel;
import wormdetector.api.json.ImageResponse;
import wormdetector.api.json.RequestParams;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
public class WormDetectorController {

    @RequestMapping(value = "/test")
    public @ResponseBody
    String test() {
        return "test ok";
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public @ResponseBody
    ImageResponse handleFormUpload(
            @RequestParam("params") String requestParamsJson,
            @RequestParam("file") MultipartFile file) throws IOException {
        if (!file.isEmpty()) {
            RequestParams requestParams = new ObjectMapper().readValue(requestParamsJson, RequestParams.class);
            WormDetectorModel model = new WormDetectorModel();
            return  model.processImg(file.getBytes(), requestParams);
        }
        return null;
    }
}
