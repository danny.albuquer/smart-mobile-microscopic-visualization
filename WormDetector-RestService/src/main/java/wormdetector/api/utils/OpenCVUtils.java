package wormdetector.api.utils;

import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.photo.Photo;

import java.util.ArrayList;

/**
 *  @author Danny Albuquerque Ferreira
 *  @date 31.01.2018
 *
 *  Cette classe simplifie les opérations de traitement d'images en cachant les appels à OpenCV.
 *  Les paramètres utilisés pour les appels des méthodes OpenCV sont propre au projet.
 */
public class OpenCVUtils {

    public OpenCVUtils() {

    }

    /**
     *  Supprime le bruit de l'image
     *  @param input Image à traiter
     *  @return Image traitée
     */
    public Mat removeNoise(Mat input) {
        Mat output = new Mat();
        Photo.fastNlMeansDenoising(input, output, 50, 7, 21);
        return output;
    }

    /**
     *  Lisse l'image
     *  @param input Image à traiter
     *  @return Image traitée
     */
    public Mat smooth(Mat input) {
        Mat output = new Mat();
        Imgproc.GaussianBlur(input, output, new Size(5, 5), 0);
        return output;
    }

    /**
     *  Binarise l'image
     *  @param input Image à traiter
     *  @return Image traitée
     */
    public Mat threshold(Mat input) {
        Mat output = new Mat();
        Imgproc.adaptiveThreshold(input, output, 255,
                Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY_INV, 201,
                30);
        Mat kernel = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE,
                new Size(10, 10));
        Imgproc.morphologyEx(output, output, Imgproc.MORPH_CLOSE, kernel); // Fill holes
        return output;
    }

    /**
     *  Trouve les contours des vers
     *  @param input Image à traiter
     *  @return ArrayList de contour sous la forme d'un MatOfPoint
     */
    public ArrayList<MatOfPoint> contours(Mat input) {
        ArrayList<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Mat hierarchy = new Mat();
        Imgproc.findContours(input, contours, hierarchy, Imgproc.RETR_EXTERNAL,
                Imgproc.CHAIN_APPROX_SIMPLE);
        return contours;
    }

    /**
     *  Trouve les boundings boxes des vers
     *  @param contours Contours des vers
     *  @return ArrayList de rectangles représentant les bounding boxes
     */
    public  ArrayList<Rect> boundingBoxes(ArrayList<MatOfPoint> contours) {
        ArrayList<Rect> boundingBoxes = new ArrayList<Rect>();
        for (int i = 0; i < contours.size(); i++) {
            Rect r = Imgproc.boundingRect(contours.get(i));
            boundingBoxes.add(r);
        }
        return boundingBoxes;
    }

    /**
     *  Trouve les boundings boxes des vers
     *  @param input Image à traiter
     *  @return  Image traitée
     */
    public Mat skeletonize(Mat input) {
        return zhangSuenThinning(input);
    }

    /**
     * Source:
     * https://github.com/Aletheios/MIME/blob/master/app/src/main/java/de/lmu/ifi/medien/mime/OpenCVUtil.java
     *
     * Implements the Zhang-Suen thinning algorithm to get the topological skeleton of a binary image
     *
     * @param img
     *           The binary image to process (filled pixels = white); method works in-place
     * @link http://dl.acm.org/citation.cfm?id=358023
     * @link http://nayefreza.wordpress.com/2013/05/11/zhang-suen-thinning-algorithm-java-implementation/
     * @link http://opencv-code.com/quick-tips/implementation-of-thinning-algorithm-in-opencv/
     * @link http://en.wikipedia.org/wiki/Topological_skeleton
     */
    public Mat zhangSuenThinning(Mat img) {
        Mat output = img.clone();
        Mat prev = Mat.zeros(output.size(), CvType.CV_8UC1);
        Mat diff = new Mat();
        do {
            zhangSuenThinningIteration(output, 0);
            zhangSuenThinningIteration(output, 1);
            Core.absdiff(output, prev, diff);
            output.copyTo(prev);
        } while (Core.countNonZero(diff) > 0);
        return output;
    }

    /**
     * Iteration step for the Zhang-Suen thinning algorithm
     *
     * @param img
     * @param step
     */
    private void zhangSuenThinningIteration(Mat img, int step) {
        // Get image pixels
        byte[] buffer = new byte[(int) img.total() * img.channels()];
        img.get(0, 0, buffer);
        byte[] markerBuffer = new byte[buffer.length];
        int rows = img.rows();
        int cols = img.cols();
        // Process all pixels
        for (int y = 1; y < rows - 1; ++y) {
            for (int x = 1; x < cols - 1; ++x) {
                // Pre-calculate offsets (indices in buffer)
                int prev = cols * (y - 1) + x;
                int cur = cols * y + x;
                int next = cols * (y + 1) + x;
                // Get 8-neighborhood of current pixel (center = p1; p2 = top middle, counting
                // clockwise)
                byte p2 = buffer[prev];
                byte p3 = buffer[prev + 1];
                byte p4 = buffer[cur + 1];
                byte p5 = buffer[next + 1];
                byte p6 = buffer[next];
                byte p7 = buffer[next - 1];
                byte p8 = buffer[cur - 1];
                byte p9 = buffer[prev - 1];
                // Get number of black-white transitions in ordered sequence of points in the
                // 8-neighborhood; note: a filled pixel (white) has a value of -1
                int a = 0;
                if (p2 == 0 && p3 == -1) {
                    ++a;
                }
                if (p3 == 0 && p4 == -1) {
                    ++a;
                }
                if (p4 == 0 && p5 == -1) {
                    ++a;
                }
                if (p5 == 0 && p6 == -1) {
                    ++a;
                }
                if (p6 == 0 && p7 == -1) {
                    ++a;
                }
                if (p7 == 0 && p8 == -1) {
                    ++a;
                }
                if (p8 == 0 && p9 == -1) {
                    ++a;
                }
                if (p9 == 0 && p2 == -1) {
                    ++a;
                }

                // Number of filled pixels in the 8-neighborhood
                int b = Math.abs(p2 + p3 + p4 + p5 + p6 + p7 + p8 + p9);
                // Condition 3 and 4
                int c3 = step == 0 ? (p2 * p4 * p6) : (p2 * p4 * p8);
                int c4 = step == 0 ? (p4 * p6 * p8) : (p2 * p6 * p8);
                // Determine if the current pixel has to be eliminated; 0 = "delete pixel", -1 =
                // "keep pixel"
                markerBuffer[cur] = (byte) ((a == 1 && b >= 2 && b <= 6 && c3 == 0
                        && c4 == 0) ? 0 : -1);
            }
        }
        // Eliminate pixels and save result
        for (int i = 0; i < buffer.length; ++i) {
            buffer[i] = (byte) ((buffer[i] == -1 && markerBuffer[i] == -1) ? -1 : 0);
        }
        img.put(0, 0, buffer);
    }

    /**
     *  Convertis un Mat en tableau de bytes
     *  @param input Image à traiter
     *  @return  Image en tableau de bytes
     */
    public byte[] matToByteArray(Mat input){
        MatOfByte bytemat = new MatOfByte();
        Imgcodecs.imencode(".jpg", input, bytemat);
        byte[] bytes = bytemat.toArray();
        return bytes;
    }

    /**
     *  Convertis une image en tableau de bytes en Mat
     *  @param input Image en tableau de bytes
     *  @return  Image en Mat
     */
    public Mat byteArrayToMat(byte[] input){
         return Imgcodecs.imdecode(new MatOfByte(input), Imgcodecs.CV_LOAD_IMAGE_UNCHANGED);
    }

    /**
     *  Mets une image en niveau de gris
     *  @param input Image à traiter
     *  @return  Image traitée
     */
    public Mat grayscale(Mat input) {
        Mat output = new Mat();
        Imgproc.cvtColor(input, output, Imgproc.COLOR_RGB2GRAY);
        return output;
    }
}
