package wormdetector.api.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 *  @author Danny Albuquerque Ferreira
 *  @date 31.01.2018
 *
 *  Cette classe représente l'objet Json des paramètres pour le traitement d'image
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "microscopezoom",
        "noise",
        "contours",
        "boundingboxes",
        "skeletons"
})
public class RequestParams {

    @JsonProperty("microscopezoom")
    private int microscopezoom;
    @JsonProperty("noise")
    private boolean noise;
    @JsonProperty("contours")
    private boolean contours;
    @JsonProperty("boundingboxes")
    private boolean boundingboxes;
    @JsonProperty("skeletons")
    private boolean skeletons;

    @JsonProperty("noise")
    public boolean isNoise() {
        return noise;
    }

    @JsonProperty("noise")
    public void setNoise(boolean noise) {
        this.noise = noise;
    }

    @JsonProperty("contours")
    public boolean isContours() {
        return contours;
    }

    @JsonProperty("contours")
    public void setContours(boolean contours) {
        this.contours = contours;
    }

    @JsonProperty("boundingboxes")
    public boolean isBoundingboxes() {
        return boundingboxes;
    }

    @JsonProperty("boundingboxes")
    public void setBoundingboxes(boolean boundingboxes) {
        this.boundingboxes = boundingboxes;
    }

    @JsonProperty("skeletons")
    public boolean isSkeletons() {
        return skeletons;
    }

    @JsonProperty("skeletons")
    public void setSkeletons(boolean skeletons) {
        this.skeletons = skeletons;
    }

    @JsonProperty("microscopezoom")
    public int getMicroscopezoom() {
        return microscopezoom;
    }

    @JsonProperty("microscopezoom")
    public void setMicroscopezoom(int microscopezoom) {
        this.microscopezoom = microscopezoom;
    }

}