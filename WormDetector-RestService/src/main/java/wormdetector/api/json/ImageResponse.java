package wormdetector.api.json;

/**
 *  @author Danny Albuquerque Ferreira
 *  @date 31.01.2018
 *
 *  Cette classe représente le retour de l'API lors d'une requête POST sur /upload
 */
public class ImageResponse {

    /**
     *  Image traitée
     */
    private byte[] image;
    /**
     *  Nombre de vers sur l'image
     */
    private int nbWorms;

    public ImageResponse(byte[] image, int nbWorms) {
        this.image = image;
        this.nbWorms = nbWorms;
    }

    public byte[] getImage() {
        return image;
    }

    public int getNbWorms() {
        return nbWorms;
    }
}
