package wormdetector.api.beans;

import java.awt.*;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

/**
 *  @author Danny Albuquerque Ferreira
 *  @date 31.01.2018
 *
 *  Cette classe représente un ver ou un ensemble de vers
 */
public class WormCC {

    /**
     *  Squelette du ver ou du cluster
     */
    private HashSet<Point2D> skeleton;

    /**
     *  Points aux extrémités du squelette
     */
    private ArrayList<Point2D> endPoints;

    /**
     *  Points visités pour la méthode récursive qui trouvent les endPoints
     */
    private HashSet<Point2D> visited;

    /**
     *  Longueur du squelette
     */
    private double length;

    /**
     *  Nombre de vers sur l'image
     */
    private int nbWorms;

    /**
     *  @param skeleton Ensemble de points représentant le squelette
     */
    public WormCC(HashSet<Point2D> skeleton) {
        this.skeleton = skeleton;
        this.endPoints = new ArrayList<Point2D>();
        findEndPoints();
        calculateLength();
        this.nbWorms = (endPoints.size() + 1) / 2;
        // nbWorms == 0 veut dire qu'il s'agit d'un ver qui boucle sur lui-même
        if(nbWorms == 0) nbWorms++;
    }

    /**
     *  Calcule la taille du squelette en faisant la somme des distances entre chacun des points
     */
    private void calculateLength() {
        length = 0;
        Iterator<Point2D> it = skeleton.iterator();
        Point2D p1 = it.next();
        while (it.hasNext()) {
            Point2D p2 = it.next();
            length += p1.distance(p2);
            p1 = p2;
        }
    }

    /**
     *  Méthode de départ pour trouver les endPoints
     */
    private void findEndPoints() {
        this.visited = new HashSet<Point2D>();
        if (!skeleton.isEmpty()) {
            findEndPoints(skeleton.iterator().next());
        }
    }

    /**
     *  Méthode récursive qui trouve les endPoints
     */
    private void findEndPoints(Point2D p) {
        if (visited.contains(p)) {
            return; // Point déjà visité
        } else {
            visited.add(p);
        }
        // Voisinage du point
        ArrayList<Point2D> neighbours = new ArrayList<Point2D>();
        for (int i = (int) p.getX() - 1; i <= (int) p.getX() + 1; i++) {
            for (int j = (int) p.getY() - 1; j <= (int) p.getY() + 1; j++) {
                if (i == p.getX() && j == p.getY()) continue;
                Point2D neighbour = new Point(i, j);
                if (skeleton.contains(neighbour)) {
                    neighbours.add(neighbour);
                    findEndPoints(neighbour);
                }
            }
        }
        // Si il n'y a qu'un voisin, il s'agit d'un endPoints
        if (neighbours.size() == 1) {
            endPoints.add(p);
        }
    }

    public double getLength() {
        return this.length;
    }

    public int getNbWorms() {
        return this.nbWorms;
    }

    public boolean isCluster(){
        return nbWorms > 1;
    }
}
