package wormdetector.api.models;

import wormdetector.api.json.ImageResponse;
import wormdetector.api.json.RequestParams;
import wormdetector.api.beans.WormCC;
import wormdetector.api.utils.OpenCVUtils;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashSet;

/**
 *  @author Danny Albuquerque Ferreira
 *  @date 31.01.2018
 *
 *  Cette classe s'occupe de faire les traitements souhaités reçus par le controller
 */
public class WormDetectorModel {

    /**
     *  Fournit les différentes opérations à effectuer sur les images
     */
    private OpenCVUtils utils;

    public WormDetectorModel() {
        utils = new OpenCVUtils();
    }

    /**
     *  Traite l'image en fonction des paramètres
     *  @param input  Image à traiter
     *  @param params Opérations à effectuer
     *  @return Image + Nombre de vers
     */
    public ImageResponse processImg(byte[] input, RequestParams params){
        // Préparation de l'image: GRAYSCALE + SMOOTH
        Mat img = utils.byteArrayToMat(input);
        Mat temp = utils.grayscale(img);
        temp = utils.smooth(temp);

        // BINARISATION
        temp = utils.threshold(temp);

        // CONTOURS
        ArrayList<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        if(params.isContours() || params.isBoundingboxes()) {
           contours = utils.contours(temp);
        }
        if(params.isContours()) {
            Imgproc.drawContours(img, contours, -1, new Scalar(255, 0, 0));
        }
        // BOUNDING BOXES
        if(params.isBoundingboxes()) {
            ArrayList<Rect> boundingBoxes = utils.boundingBoxes(contours);
            for (Rect r : boundingBoxes) {
                Imgproc.rectangle(img, r.tl(), r.br(), new Scalar(0, 255, 0));
            }
        }

        // SKELETONIZATION
        Mat skeletons = utils.skeletonize(temp);
        ArrayList<WormCC> worms = detectWorms(skeletons);
        Imgproc.cvtColor(skeletons, skeletons, Imgproc.COLOR_GRAY2RGB);
        if(params.isSkeletons()) {
            skeletons.copyTo(img,skeletons);
        }

        // NUMBER OF WEORMS
        int nbWorms = 0;
        for (WormCC w: worms) {
            nbWorms += w.getNbWorms();
        }

        return new ImageResponse(utils.matToByteArray(img), nbWorms);
    }


    /**
     *  Cette méthode détecte les vers à partir de leurs squelettes
     *  @param skeletons  Squelettes trouvés
     *  @return Les vers sous forme de connected components
     */
    public ArrayList<WormCC> detectWorms(Mat skeletons){
        Mat labels = new Mat();
        // Trouve les différents connect components (CC)
        int nbLabels = Imgproc.connectedComponents(skeletons, labels);
        // Récupére tous les points pour chacun des CC
        ArrayList<HashSet<Point2D>> connectedComponents = new ArrayList<HashSet<Point2D>>();
        for (int i = 0; i < nbLabels-1; i++) {
            connectedComponents.add(new HashSet<Point2D>());
        }
        for (int i = 0; i < labels.rows(); i++) {
            for (int j = 0; j < labels.cols(); j++) {
                int value = (int) labels.get(i, j)[0];
                if (value > 0)
                    connectedComponents.get(value - 1).add(new Point(j, i));
            }
        }
        // Représente chacun des vers dans des WormCC
        ArrayList<WormCC> wormCCs = new ArrayList<WormCC>();
        for (HashSet<Point2D> connectedComponent : connectedComponents) {
            if(connectedComponent.size() > 20)
                wormCCs.add(new WormCC(connectedComponent));
        }
        return wormCCs;
    }


}
