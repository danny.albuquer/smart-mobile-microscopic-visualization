import com.fasterxml.jackson.databind.ObjectMapper;
import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

public class Application {

    private static final String BD_PATH = "./src/main/resources/celegans-db/bd.json";
    private static final String BD_RESULT_PATH = "./src/main/resources/celegans-db/bd-results.json";
    //private static final String IMAGE_PATH = "./src/main/resources/celegans-db/";
    private static final String IMAGE_PATH = "E:\\smart-mobile-microscopic-visualization\\WormDetectorTester\\src\\main\\resources\\celegans-db\\";

    public static void main(String[] args){
        // Load OpenCV
        nu.pattern.OpenCV.loadShared();

        // Load DB
        ObjectMapper mapper = new ObjectMapper();
        WormsDB wormsDB = null;
        //JSON from file to Object
        try {
            wormsDB = mapper.readValue(new File(BD_PATH), WormsDB.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ArrayList<Image> results = new ArrayList<Image>();
        // Processes all images
        for (Image image: wormsDB.getImages()) {
            String imgPath = IMAGE_PATH + image.getFilename();
            Mat img = Imgcodecs.imread(imgPath,0);
            Image imgResult = new Image();
            imgResult.setFilename(image.getFilename());
            if(!img.empty()){
                try{
                    int nbWorms = processImg(img);
                    imgResult.setNbWorms(nbWorms);
                }catch(Exception e){
                    imgResult.setNbWorms(-1);
                }
            }else{
                imgResult.setNbWorms(-1);
            }
            results.add(imgResult);
        }

        WormsDB resultDB = new WormsDB();
        resultDB.setImages(results);
        //Object to JSON in file
        try {
            mapper.writeValue(new File(BD_RESULT_PATH), resultDB);
        } catch (IOException e) {
            e.printStackTrace();
        }
        stats(wormsDB,resultDB);
    }

    public static int processImg(Mat img) {
        OpenCVUtils utils = new OpenCVUtils();
        Mat temp = img.clone();
        temp = utils.smooth(temp);
        temp = utils.threshold(temp);
        Mat skeletons = utils.zhangSuenThinning(temp);
        ArrayList<WormCC> worms = detectWorms(skeletons);
        int nbWorms = 0;
        for (WormCC w: worms) {
            nbWorms += w.getNbWorms();
        }
        return nbWorms;
    }

    public static ArrayList<WormCC> detectWorms(Mat skeletons){
        Mat labels = new Mat();
        int nbLabels = Imgproc.connectedComponents(skeletons, labels);
        ArrayList<HashSet<Point2D>> connectedComponents = new ArrayList<HashSet<Point2D>>();
        for (int i = 0; i < nbLabels-1; i++) {
            connectedComponents.add(new HashSet<Point2D>());
        }
        for (int i = 0; i < labels.rows(); i++) {
            for (int j = 0; j < labels.cols(); j++) {
                int value = (int) labels.get(i, j)[0];
                if (value > 0)
                    connectedComponents.get(value - 1).add(new Point(j, i));
            }
        }
        ArrayList<WormCC> wormCCs = new ArrayList<WormCC>();
        for (HashSet<Point2D> connectedComponent : connectedComponents) {
            if(connectedComponent.size() > 20)
                wormCCs.add(new WormCC(connectedComponent));
        }
        return wormCCs;
    }

    private static void stats(WormsDB source, WormsDB results){
        double nbImg = source.getImages().size();
        double nbError = 0;
        double diffError = 0;
        double errorPercentsSum = 0;
        for (int i = 0; i < nbImg; i++) {
            double nbWormsSource = source.getImages().get(i).getNbWorms();
            double nbWormsResult = results.getImages().get(i).getNbWorms();
            if(nbWormsSource != nbWormsResult){
                nbError++;
                diffError += Math.abs(nbWormsSource-nbWormsResult);
                double errorPercent = Math.abs(nbWormsSource-nbWormsResult)/nbWormsSource * 100.0;
                errorPercentsSum += errorPercent;
                System.out.println(source.getImages().get(i).getFilename() + " - Error percent: " + errorPercent + "%");
            }
        }
        System.out.println("Error percent mean: " + (errorPercentsSum/nbImg) + "%");
        System.out.println("Diff mean: " + (diffError/nbError) + "%");
    }

}
