import java.awt.*;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

public class WormCC {

    private HashSet<Point2D> skeleton;
    private ArrayList<Point2D> endPoints;
    private HashSet<Point2D> visited;
    private double length;
    private int nbWorms;

    public WormCC(HashSet<Point2D> skeleton) {
        this.skeleton = skeleton;
        this.endPoints = new ArrayList<Point2D>();
        findEndPoints();
        calculateLength();
        this.nbWorms = (endPoints.size() + 1) / 2;
        if(nbWorms == 0) nbWorms++;
    }

    private void calculateLength() {
        length = 0;
        Iterator<Point2D> it = skeleton.iterator();
        Point2D p1 = it.next();
        while (it.hasNext()) {
            Point2D p2 = it.next();
            length += p1.distance(p2);
            p1 = p2;
        }
    }

    private void findEndPoints() {
        this.visited = new HashSet<Point2D>();
        if (!skeleton.isEmpty()) {
            findEndPoints(skeleton.iterator().next());
        }
    }

    private void findEndPoints(Point2D p) {
        if (visited.contains(p)) {
            return;
        } else {
            visited.add(p);
        }
        ArrayList<Point2D> neighbours = new ArrayList<Point2D>();
        for (int i = (int) p.getX() - 1; i <= (int) p.getX() + 1; i++) {
            for (int j = (int) p.getY() - 1; j <= (int) p.getY() + 1; j++) {
                if (i == p.getX() && j == p.getY()) continue;
                Point2D neighbour = new Point(i, j);
                if (skeleton.contains(neighbour)) {
                    neighbours.add(neighbour);
                    findEndPoints(neighbour);
                }
            }
        }
        if (neighbours.size() == 1) {
            endPoints.add(p);
        }
    }

    public double getLength() {
        return this.length;
    }

    public int getNbWorms() {
        return this.nbWorms;
    }

    public boolean isCluster(){
        return nbWorms > 1;
    }
}
