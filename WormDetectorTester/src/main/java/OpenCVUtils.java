import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.photo.Photo;

import java.util.ArrayList;

public class OpenCVUtils {

    public OpenCVUtils() {

    }

    public Mat removeNoise(Mat input) {
        Mat output = new Mat();

        Photo.fastNlMeansDenoising(input, output, 50, 7, 21);

        return output;
    }

    public Mat smooth(Mat input) {
        Mat output = new Mat();

        Imgproc.GaussianBlur(input, output, new Size(5, 5), 0);

        return output;
    }

    public Mat threshold(Mat input) {
        Mat output = new Mat();

        Imgproc.adaptiveThreshold(input, output, 255,
                Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY_INV, 201,
                30);
        Mat kernel = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE,
                new Size(10, 10));
        Imgproc.morphologyEx(output, output, Imgproc.MORPH_CLOSE, kernel); // Fill holes

        return output;
    }

    public ArrayList<MatOfPoint> contours(Mat input) {
        ArrayList<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Mat hierarchy = new Mat();
        Imgproc.findContours(input, contours, hierarchy, Imgproc.RETR_CCOMP,
                Imgproc.CHAIN_APPROX_SIMPLE);

        return contours;
    }

    public  ArrayList<Rect> boundingBoxes(ArrayList<MatOfPoint> contours) {
        ArrayList<Rect> boundingBoxes = new ArrayList<Rect>();

        for (int i = 0; i < contours.size(); i++) {
            Rect r = Imgproc.boundingRect(contours.get(i));
            boundingBoxes.add(r);
        }

        return boundingBoxes;
    }
    public Mat skeletonize(Mat input) {
        return zhangSuenThinning(input);
    }

    /**
     * Source:
     * https://github.com/Aletheios/MIME/blob/master/app/src/main/java/de/lmu/ifi/medien/mime/OpenCVUtil.java
     *
     * Implements the Zhang-Suen thinning algorithm to get the topological skeleton of a binary image
     *
     * @param img
     *           The binary image to process (filled pixels = white); method works in-place
     * @link http://dl.acm.org/citation.cfm?id=358023
     * @link http://nayefreza.wordpress.com/2013/05/11/zhang-suen-thinning-algorithm-java-implementation/
     * @link http://opencv-code.com/quick-tips/implementation-of-thinning-algorithm-in-opencv/
     * @link http://en.wikipedia.org/wiki/Topological_skeleton
     */
    public Mat zhangSuenThinning(Mat img) {
        Mat output = img.clone();
        Mat prev = Mat.zeros(output.size(), CvType.CV_8UC1);
        Mat diff = new Mat();
        do {
            zhangSuenThinningIteration(output, 0);
            zhangSuenThinningIteration(output, 1);
            Core.absdiff(output, prev, diff);
            output.copyTo(prev);
        } while (Core.countNonZero(diff) > 0);
        return output;
    }

    /**
     * Iteration step for the Zhang-Suen thinning algorithm
     *
     * @param img
     * @param step
     */
    private void zhangSuenThinningIteration(Mat img, int step) {
        // Get image pixels
        byte[] buffer = new byte[(int) img.total() * img.channels()];
        img.get(0, 0, buffer);

        byte[] markerBuffer = new byte[buffer.length];

        int rows = img.rows();
        int cols = img.cols();

        // Process all pixels
        for (int y = 1; y < rows - 1; ++y) {
            for (int x = 1; x < cols - 1; ++x) {
                // Pre-calculate offsets (indices in buffer)
                int prev = cols * (y - 1) + x;
                int cur = cols * y + x;
                int next = cols * (y + 1) + x;

                // Get 8-neighborhood of current pixel (center = p1; p2 = top middle, counting
                // clockwise)
                byte p2 = buffer[prev];
                byte p3 = buffer[prev + 1];
                byte p4 = buffer[cur + 1];
                byte p5 = buffer[next + 1];
                byte p6 = buffer[next];
                byte p7 = buffer[next - 1];
                byte p8 = buffer[cur - 1];
                byte p9 = buffer[prev - 1];

                // Get number of black-white transitions in ordered sequence of points in the
                // 8-neighborhood; note: a filled pixel (white) has a value of -1
                int a = 0;
                if (p2 == 0 && p3 == -1) {
                    ++a;
                }
                if (p3 == 0 && p4 == -1) {
                    ++a;
                }
                if (p4 == 0 && p5 == -1) {
                    ++a;
                }
                if (p5 == 0 && p6 == -1) {
                    ++a;
                }
                if (p6 == 0 && p7 == -1) {
                    ++a;
                }
                if (p7 == 0 && p8 == -1) {
                    ++a;
                }
                if (p8 == 0 && p9 == -1) {
                    ++a;
                }
                if (p9 == 0 && p2 == -1) {
                    ++a;
                }

                // Number of filled pixels in the 8-neighborhood
                int b = Math.abs(p2 + p3 + p4 + p5 + p6 + p7 + p8 + p9);

                // Condition 3 and 4
                int c3 = step == 0 ? (p2 * p4 * p6) : (p2 * p4 * p8);
                int c4 = step == 0 ? (p4 * p6 * p8) : (p2 * p6 * p8);

                // Determine if the current pixel has to be eliminated; 0 = "delete pixel", -1 =
                // "keep pixel"
                markerBuffer[cur] = (byte) ((a == 1 && b >= 2 && b <= 6 && c3 == 0
                        && c4 == 0) ? 0 : -1);
            }
        }

        // Eliminate pixels and save result
        for (int i = 0; i < buffer.length; ++i) {
            buffer[i] = (byte) ((buffer[i] == -1 && markerBuffer[i] == -1) ? -1
                    : 0);
        }
        img.put(0, 0, buffer);
    }

    public byte[] matToByteArray(Mat input){
//        // Mat to byte array
//        byte[] output = new byte[(int) (input.total()
//                * input.channels())];
//        input.get(0, 0, output);
//
//        return output;

        MatOfByte bytemat = new MatOfByte();

        Imgcodecs.imencode(".jpg", input, bytemat);

        byte[] bytes = bytemat.toArray();

        return bytes;
    }

    public Mat byteArrayToMat(byte[] input){
//        Mat output = new Mat(220, 160, CvType.CV_8UC3);
//        output.put(0, 0, input);
//        return output;
         return Imgcodecs.imdecode(new MatOfByte(input), Imgcodecs.CV_LOAD_IMAGE_UNCHANGED);
    }

    public Mat grayscale(Mat img) {
        Mat output = new Mat();
        Imgproc.cvtColor(img, output, Imgproc.COLOR_RGB2GRAY);
        return output;
    }
}
