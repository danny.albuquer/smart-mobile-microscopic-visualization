import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "filename",
        "nbWorms"
})
public class Image {

    @JsonProperty("filename")
    private String filename;
    @JsonProperty("nbWorms")
    private int nbWorms;

    @JsonProperty("filename")
    public String getFilename() {
        return filename;
    }

    @JsonProperty("filename")
    public void setFilename(String filename) {
        this.filename = filename;
    }

    @JsonProperty("nbWorms")
    public int getNbWorms() {
        return nbWorms;
    }

    @JsonProperty("nbWorms")
    public void setNbWorms(int nbWorms) {
        this.nbWorms = nbWorms;
    }

}